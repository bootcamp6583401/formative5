1. Bagaimana Anda menangani exception dalam Java?
   Menggunakan try-catch block. Dimana code yang dianggap dapat menghasilkan atau throw error diletakkan dalam blok try dan catch blocknya digunakkan
   untuk mengangkap Exception yang dihasilkan kode di block try.

```java
try {
    // Code yang menghasilkan error
} catch(Exception e) {

}
```

2. Apa perbedaan antara checked exception dan unchecked exception?
   Checked exception adalah exception yang dicek pada saat compile time. Maksudnya java program tidak akan jalan sebelum error ini ditangani. Contohnya adalah membaca file yang tidak ada. Compile akan error jika file tidak ditemukan.

   Sementara unchecked exception adalah exception yang terjadi tetapi tidak mencegah compilasi. Misalnya mengambil index suatu ArrayList secara dimanis (scanner misalnya) dan menggunakan index tersebut untuk mengakses elemen array list. Jika melebihi akan error. Tapi tidak terjadi saat compile.

3. Jelaskan penggunaan blok try, catch, dan finally dalam penanganan exception.
   Blok try adalah tempat menaruh kode yang kemungkinan throw exception.

   Catch merpakan blok yang menangani Exception jika di throw.

   Finally adalah kode block yang akan pasti jalan, apapun hasil dari kode blok _try_. BIasanya untuk menutup object, atau cleanup.

4. Apa itu throws dalam deklarasi metode? Bagaimana cara menggunakannya?
   Jika metode deklarasi bahwa dia _throws_ exception, ini akan memberi tahu yang memanggil bahwa metode ini mungkin menghasilkan error. Ini membantu developer mengantisipasi error. Contohnya adalah `Thread.sleep`. Metode ini harus diletakkan di try catch block

5. Bagaimana Anda membuat kelas exception kustom di Java?
   Dengan extend class Exception dan memanggil constructor parent.

```java
public class MyCustomException extends Exception {
    public MyCustomException(){
        super("Custom Message");
    }
}
```
