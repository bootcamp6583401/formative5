1. Apa itu unit testing, dan mengapa unit testing penting dalam pengembangan perangkat lunak?
   Unit testing adalah testing dimana komponen individual semua di test. Gunannya adalah mencegah error, menyediakkan dokumentasi, dan memastikan
   software selalu berjalan dan tidak ada edge case walaupun tambah fitur atau code.

2. Jelaskan konsep "Arrange, Act, Assert" (AAA) dalam konteks pengujian unit.

- Arrange; Setup semua yang diperlukan untuk menjalankan test, seperti mock, etc.
- Act: menjalankan test. Memanggil metode yang diperlukan, etc.
- Assert: mementukan apakah hasil Act benar apa tidak

3. Bagaimana Anda dapat mengidentifikasi apakah suatu metode atau fungsi memerlukan pengujian unit?
   Parameter setiap tim mungkin berbeda, tapi seharusnya dicek kompleksitas metode. Misalnya jika banyak if-else. Pastikan tes dengan banyak kondisi berbeda untuk mencegah edge case.

4. Apa keuntungan menggunakan kerangka pengujian (testing framework) seperti JUnit?

- Mengikuti standar: developer dapat mengikuti standar yang sudah digunakkan banyak orang.
- Developer juga dapat membuat test dengan lebih cepat.
- Automation

5. Bagaimana mocking dapat membantu dalam pengujian unit?

Mocking dapat membantu test yang perlu dependency yang mahal dan lama seperti connect ke databse. Mocking juga dapat simulasi kelas yang kompleks.
