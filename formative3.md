1. Apa perbedaan antara JUnit 4 dan JUnit 5?
   JUnit 5 banyak menambahkan anotasi untuk membantu testing seperti @BeforeEach.

2. Bagaimana cara menggunakan Mockito untuk membuat mock objects dalam pengujian?

- Masukkan Mockito ke dalam dependency
- buat kelas palsu
- assert

```java
MyClass myMockedClass = spy(new MyClass());

when(myMockedClass.myMethod()).thenReturn("Harus Sama");

assertEquals("Harus Sama", myMockedClass.myMethod());
```

3. Apa yang dimaksud dengan "parameterized tests" dalam JUnit?
   Mengetes metode dengan beberapa parameter untuk memastikan metode berjalan dengan benar. Mungkin bisa digunakkan untuk menemukan edge cases.

4. Bagaimana menggunakan assertJ untuk meningkatkan pembacaan dan penulisan pengujian?

```java
import static org.assertj.core.api.Assertions.assertThat;

public class CalculatorTest {

    @org.junit.jupiter.api.Test
    public void testAddition() {
        Calculator calculator = new Calculator();
        assertThat(calculator.add(2, 3)).isEqualTo(5);
    }
}

class Calculator {
    public int add(int a, int b) {
        return a + b;
    }
}
```

    Dalam contoh ini, test terbaca dengan flow yang sangat lancar "Assert that calculator.add method is equal to bla bla"

5. Apa kegunaan TestNG dan bagaimana cara menggunakannya dalam pengujian?
   Menggunakan @Test untuk menandakkan bahwa suatu metode adalah test.

   Setup menggunaan @BeforeSuite

   Cleanup menggunakan @AfterSuite
