1. Apa tujuan dari logging dalam pengembangan perangkat lunak?

- Monitor dan diagnosa
  Developer/team member lainnya dapat mengetahui hal penting yang terjadi dan kapan
- Debugging
  Bisa untuk menulis error error yang terjadi untuk diperbaiki
- Audit
  Bisa juga untuk mengikuti policy yang ada pada proyek tersebut. Dengan logging bisa dilacak siapa yang melakakukan apa dan kapan
- Performance
  Memonitor performa software

2. Sebutkan beberapa level logging yang umum digunakan dalam log4j atau Java Logging API.

- TRACE
- DEBUG
- INFO
- WARN
- ERROR
- SEVERE

3. Bagaimana Anda mengonfigurasi sistem logging di aplikasi Java Anda?
   Java memiliki Logger built in. Bisa dikonfigurasi seperti:

```java
Logger logger = Logger.getLogger("com.example.MyClass");
logger.setLevel(Level.INFO);
```

4. Jelaskan perbedaan antara logging sinkron dan asinkron.
   Sinkron adalah blocking, jadi saat log ditulis aplikasi akan terhenti. Kebalikkannya adalah asinkron. Walaupun asinkron tidak blocking
   tetap ada resikonya seperti log tidak tertulis jika aplikasi crash.

5. Bagaimana Anda dapat mencegah log statements yang mahal secara komputasi dari dieksekusi jika tingkat log tidak aktif?
   Dengan cek "severity" lognya. Tadi di no 3 sudah di configure untuk set level info ke atas. Sekarang bisa dicek sebelum log:

```java
if (logger.isLoggable(Level.INFO)) {
    logger.info(": Mahal BRO" + logMahal());
}
```
